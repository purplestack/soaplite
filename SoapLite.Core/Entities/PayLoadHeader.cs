﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapLite.Core.Entities
{
    [JsonObject]
    [Serializable]
    public class PayLoadHeader
    {
            public string Key { get; set; }
            public string Value { get; set; }
    }
}
