﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapLite.Core.Entities
{
    public class AddAPISecResponse : BaseResponse
    {
        public APISecuritySpec ApiSecurity { get; set; }
    }
}
