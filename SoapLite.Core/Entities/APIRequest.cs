﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapLite.Core.Entities
{
    [JsonObject]
    [Serializable]
    public class APIRequest
    {
        public string EndPointURL { get; set; }
        public AuthenticationData AuthData { get; set; }
        public PayLoadBody Context { get; set; }
        public string HttpMethod { get; set; }
    }
}
