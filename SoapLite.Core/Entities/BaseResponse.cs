﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapLite.Core.Entities
{
    /// <summary>
    /// Inherited response attributes for all response messages
    /// </summary>
    public class BaseResponse
    {
        /// <summary>
        /// Generic response code for all request relating to the platfrom
        /// </summary>
        public string ResponseCode { get; set; }
        /// <summary>
        /// Generic response description for response codes accross the platform
        /// </summary>
        public string ResponseDescription { get; set; }
    }
}
