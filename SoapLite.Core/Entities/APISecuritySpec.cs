﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SoapLite.Core.Utility.Enumerations;

namespace SoapLite.Core.Entities
{
    public class APISecuritySpec
    {
        [Key]
        public string ID { get; set; }
        public string URL { get; set; }
        public string SecurityMetric { get; set; }
        public WSDLSourceFormat SourceFileFormat { get; set; }
        public bool isSoap { get; set; }
        public string ServiceDescription { get; set; }
    }

}
