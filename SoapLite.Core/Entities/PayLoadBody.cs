﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapLite.Core.Entities
{
    [JsonObject]
    [Serializable]
    public class PayLoadBody
    {
        public bool IsSoap { get; set; }
        public string Body { get; set; }
        public PayLoadHeader[] Headers { get; set; }
    }
}
