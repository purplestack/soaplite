﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapLite.Core.Entities
{
    /// <summary>
    /// Instruction for a soap request payload 
    /// </summary>
    [JsonObject]
    [Serializable]
    public class Instruction
    {
        //Endpoint url for API
        public string EndpointURL { get; set; }
        //Boolean value to determine if API is SOAP or REST
        public bool IsSoapAPI { get; set; }
    }
}
