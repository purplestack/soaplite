﻿using SoapLite.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapLite.Core
{
    public interface IProxyRequestAuthentication
    {
        Task<APIResponse> RunAuthentication(APIRequest Request);
    }
}
