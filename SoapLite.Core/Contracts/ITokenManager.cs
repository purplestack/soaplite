﻿using SoapLite.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapLite.Core.Contracts
{
    public interface ITokenManager
    {
        string ExtendToken(string CustomerID);

        Task<APIAuthenticationResponse> NonTransactionalTokenAuthentication(BaseRequest TokenValidationRequest);

        Task<APIAuthenticationResponse> TransactionalTokenAuthentication(BaseRequest TokenValidationRequest);
    }
}
