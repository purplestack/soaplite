﻿using SoapLite.Core;
using SoapLite.Core.Entities;
using SoapLite.Persistence;
using SoapLite.Service.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapLite.Service
{
    public class AuthenticationService : IProxyRequestAuthentication
    {
        /// <summary>
        /// Sefault implementation to Authenticate proxy api requests
        /// </summary>
        /// <param name="proxyRequest"></param>
        /// <returns></returns>
        public Task<APIResponse> RunAuthentication(APIRequest proxyRequest)
        {
            Logger.LogInfo("ProxyRequestAuthentication.RunAuthentication.Input", proxyRequest);
            APIResponse response = null;
            APIAuthenticationResponse authResponse = new APIAuthenticationResponse();
            try
            {
                ContextRepository<APISecuritySpec> repository = new ContextRepository<APISecuritySpec>();
                ApplicationDbContext context = new ApplicationDbContext();

                APISecuritySpec securitySpec = context.APISecuritySpecs.SqlQuery(string.Format("select * from APISecuritySpecs where URL like '%{0}%' ", proxyRequest.EndPointURL)).ToList().FirstOrDefault();
                if (securitySpec != null)
                {
                    string[] authentications = securitySpec.SecurityMetric.Split(new Char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    Logger.LogInfo("ProxyRequestAuthentication.RunAuthentication.tracker", authentications);
                    if (authentications.Contains("Token"))
                    {

                        response = new APIResponse
                        {
                            ResponseCode = authResponse.ResponseCode,
                            ResponseDescription = authResponse.ResponseDescription
                        };
                    }
                    else if (authentications.Contains("Open"))
                    {
                        Logger.LogInfo("ProxyRequestAuthentication.RunAuthentication", "Open service");
                        response = new APIResponse
                        {
                            ResponseCode = "00",
                            ResponseDescription = "no authentication required"
                        };
                    }
                    else
                    {
                        response = new APIResponse
                        {
                            ResponseCode = authResponse.ResponseCode,
                            ResponseDescription = authResponse.ResponseDescription
                        };
                    }

                }
                else
                {
                    response = new APIResponse
                    {
                        ResponseCode = "",
                        ResponseDescription = "No authentication configured for this service"
                    };
                }

            }
            catch (Exception ex)
            {
                response = new APIResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "An error occured while authenticating: " + ex.Message
                };
            }

            Logger.LogInfo("ProxyRequestAuthentication.RunAuthentication.Response", response);
            return Task.Run(() => response);
        }
    }
}
