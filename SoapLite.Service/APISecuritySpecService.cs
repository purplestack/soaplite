﻿using SoapLite.Core.Entities;
using SoapLite.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapLite.Service
{
    public class APISecuritySpecService
    {
        public AddAPISecResponse AddAPISecuritySpec(APISecuritySpec request)
        {
            APISecuritySpecRepository repository = new APISecuritySpecRepository();
            AddAPISecResponse response = new AddAPISecResponse();
            var existingEntity = repository.Get(request.ID);

            if (existingEntity == null)
            {
                repository.Update(request);
                response = new AddAPISecResponse
                {
                    ApiSecurity = repository.Get(request.ID),
                    ResponseCode = "00",
                    ResponseDescription = "Update successful",
                };
            }
            else
            {
                response = new AddAPISecResponse
                {
                    ResponseCode = "0x01",
                    ResponseDescription = "API does not exist",
                };
            }
            return response;
        }

        public AddAPISecResponse UpdateApiSecuritySpec(APISecuritySpec request)
        {
            APISecuritySpecRepository repository = new APISecuritySpecRepository();
            AddAPISecResponse response = new AddAPISecResponse();
            var existingEntity = repository.Get(request.ID);

            if (existingEntity != null)
            {
                repository.Update(request);
                response = new AddAPISecResponse
                {
                    ApiSecurity = repository.Get(request.ID),
                    ResponseCode = "00",
                    ResponseDescription = "Update successful",
                };
            }
            else
            {
                response = new AddAPISecResponse
                {
                    ResponseCode = "0x01",
                    ResponseDescription = "API does not exist",
                };
            }
            return response;
        }
    }
}
